const { Schema, model} = require('mongoose');

const BookSchema = new Schema({
    title:{ type: String, required: true},
    autor: {type: String, required: true},
    isbn: {type:String, required: true},
    imagenPath: { type: String },
    create_at: {type: Date, default: Date.now}
});

module.export = model('Book', BookSchema);