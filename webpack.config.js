const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const devMode = process.env.NODE_ENV !== 'production';
console.log(devMode)

module.exports = {
    
    entry: './Frontend/app.js',
    output: {
        path: path.join(__dirname, 'Backend/public'),
        filename: 'js/bundle.js'
    },
    mode: 'development',

module: {
  rules: [
    {
      test: /\.css/,
      use: [
        devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
        'css-loader'        
      ]
    }
  ]
},

 plugins: [
    new HtmlWebpackPlugin({
        filename: './Fronted/index.html',
        minify: {
          collapseWhitespace: true,
          removeComments: true,
          removeRundantAttributes: true,
          removeScriptTypeAttributes: true, 
          removeStyleLinkAttributes: true,
          useShortDoctype: true
        }
    }),
    new MiniCssExtractPlugin({
      filename: 'css/bundle.css'
    })
  ],
  devtool: 'source-map'
};