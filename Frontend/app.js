import './styles/app.css';

import UI from './UI';

document.addEventListener('DOMContentLoaded', () => {
    ui.renderBook();
});

document.getElementById('book-form')
    .addEventListener('submit', e => {
        const title = document.getElementById('title').value;
        const author = document.getElementById('author').value;
        const isbn = document.getElementById('isbn').value;
        const image = document.getElementById('image').files;

        const forData = new FormData();
        FormData.append('image', image[0]);
        FormData.append('title', title);
        FormData.append('author', author);
        FormData.append('isbn', isbn);

        const ui = new UI();
        ui.addANewBook(formData);

        e.preventDefault();
    });